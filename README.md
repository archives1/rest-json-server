# rest-json-server
A simple fake REST API using json-server

# Run : 
# 1. "npm install" 
# 2. "npm run rest-run"

## Goto https://localhost:3000

![konsole](http://i.imgur.com/S9yIvC9.png)

![homepage](http://i.imgur.com/HhA72V2.png)

![get-request](http://i.imgur.com/2uoRTud.png)

![post-request](http://i.imgur.com/4EsqbLf.png)

![after-post](http://i.imgur.com/R13dcUV.png)
